import 'package:flutter_questionaire/main.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
    testWidgets('Test the UI of Radio Button Options', (WidgetTester tester) async {

        await tester.pumpWidget(App());
        //Find an option with text of "Asset Management" and click the option.
        expect(find.text('Asset Management'), findsOneWidget);

        //Trigger UI update after clicking the option.
        await tester.tap(find.text('Asset Management'));
        await tester.pump();

        //Find an option with text of "More than 10,000" and click the option.
        expect(find.text('More than 10,000'), findsOneWidget);

        //Trigger UI update after clicking the option.
        await tester.tap(find.text('More than 10,000'));
        await tester.pump();

        //Find an option with text of "Europe" and click the option.
        expect(find.text("Europe"), findsOneWidget);

        //Trigger UI update after clicking the option.
        await tester.tap(find.text('Europe'));
        await tester.pump();

        //Find an option with text of "6-9 months" and click the option.
        expect(find.text("6-9 months"), findsOneWidget);

        //Trigger UI update after clicking the option.
        await tester.tap(find.text('6-9 months'));
        await tester.pump();

        //After all the options have been clicked from the test, look for the following texts:
        expect(find.text('What is the nature of you business needs?'), findsOneWidget);
        expect(find.text('Asset Management'), findsOneWidget);
        expect(find.text('What is the expected size of the user base?'), findsOneWidget);
        expect(find.text('More than 10,000'), findsOneWidget);
        expect(find.text('In which region would the majority of the user base be?'), findsOneWidget);
        expect(find.text("Europe"), findsOneWidget);
        expect(find.text('What is the expected project duration?'), findsOneWidget);
        expect(find.text("6-9 months"), findsOneWidget);

    });
}
