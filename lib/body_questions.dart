import 'package:flutter/material.dart';

import './answer_button.dart';

class BodyQuestions extends StatelessWidget {
    final questions;
    final int questionIdx;
    final Function nextQuestion;

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });
    
    @override
    Widget build(BuildContext context) {
        Text questionText = Text(
            questions[questionIdx]['question'].toString(),
            style: TextStyle(
                fontSize: 24.0
            ),
        );

        var options = questions[questionIdx]['options'] as List<String>;
       
        var answerOptions = options.map((String option) {
            return AnswerButton(text: option, nextQuestion: nextQuestion);
        });

        return Container(
            width: double.infinity,
            padding: EdgeInsets.all(16),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    questionText,
                    ...answerOptions,
                    Container(
                        margin: EdgeInsets.only(top: 30),
                        width: double.infinity,
                        child: ElevatedButton(
                            child: Text('Skip Question'),
                            onPressed: () => nextQuestion(null)
                        ),
                    )
                ]
            ),
        );
    }
}

//we use function Data type id we are going to define a function - Function
//if we are receiving a function from somewhere else(outside) we can simply add parenthesis - Function()

//App.questionIdx = 2
// => BodyQuestions.questionIdx = 2